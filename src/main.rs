mod action;
mod commands;
mod editor;
mod repository;

use clap::{crate_authors, crate_description, crate_version, App, AppSettings::*, Arg, SubCommand};

use colored::*;
use directories::ProjectDirs;
use git2::{ErrorCode, Repository};

fn main() {
    // Setup cli commands/args structure and get matches
    let matches = base_settings(App::new("octopus-cli"))
        .setting(ArgRequiredElseHelp)
        .version(crate_version!())
        .author(crate_authors!())
        .about(crate_description!())
        .subcommand(
            base_settings(SubCommand::with_name("init"))
                .about("Initialize Octopus repo for both development and management.")
                .arg(Arg::with_name("force")
                     .long("force")
                     .help("Force the initialization of Octopus, rewriting it. Use with caution."))
                .arg(Arg::with_name("modules")
                     .long("modules")
                     .short("m")
                     .help("Specifies one or more modules to be initialized with Octopus. If not specified, all modules will be downloaded.")
                     .takes_value(true)
                     .multiple(true)
                     .possible_values(&["actions", "core", "infra"])
                     .conflicts_with("select"))
                .arg(Arg::with_name("select")
                     .long("select")
                     .short("s")
                     .help("Select one or more modules to be initialized with Octopus.")))
        .subcommand(
            base_settings(SubCommand::with_name("action"))
                .about("Manage actions")
                .setting(ArgRequiredElseHelp)
                .subcommand(
                    base_settings(SubCommand::with_name("list"))
                        .about("List all Octopus actions on current system. If the current system branch is behind remote, it's going to pull the remote. If the current system branch diverges from remote, it's going to fail with a warning, unless --local is passed.")
                        .arg(Arg::with_name("local")
                             .short("l")
                             .long("local")
                             .help("Skip remote check and print only locally available actions."))
                        .arg(Arg::with_name("namespace")
                             .short("n")
                             .long("namespace")
                             .help("Specify a namespace to list instead of listing all the actions.")
                             .takes_value(true)))
                .subcommand(
                    base_settings(SubCommand::with_name("validate"))
                        .about("Validate all Octopus actions on current system, and print errors if any. This command is executed automatically before deployment so invalid actions won't be deployed.")
                        .arg(Arg::with_name("verbose")
                             .short("v")
                             .long("verbose")
                             .help("Show all info available about the errors detected.")))
                .subcommand(
                    base_settings(SubCommand::with_name("show"))
                        .about("Shows more detailed info about the action, enough to be called by Octopus API.")
                        .arg(Arg::with_name("local")
                             .short("l")
                             .long("local")
                             .help("Skip remote check and print only locally available actions."))
                        .arg(Arg::with_name("action name")
                             .help("The action name to be shown.")
                             .required(true))))
        .get_matches();

    // Check for octopus repo on data_dir
    if let Some(proj_dirs) = ProjectDirs::from("com", "mateusfccp", "octopus") {
        // Get data_dir
        let data_dir = proj_dirs.data_dir();

        // `init` command has priority over anything
        if matches.subcommand_name() == Some("init") {
            commands::init::main(matches.subcommand_matches("init"), data_dir);
        }

        // Try to open or clone Octopus repository
        let repo = Repository::open(data_dir);

        if let Err(err) = &repo {
            if err.code() == ErrorCode::NotFound {
                println!("{}", "Octopus was not found on your system.".red());
            } else {
                println!("An unexpected error ocurred: {}", err.message());
            }

            commands::init::main(None, data_dir);
        };

        match matches.subcommand_name() {
            Some("action") => {
                commands::action::main(matches.subcommand_matches("action"), repo.unwrap())
            }
            _ => (),
        };
    } else {
        println!("Couldn't define your home directory! Are you sure that HOME env var is defined?");
    }
}

fn base_settings<'a, 'b>(app: App<'a, 'b>) -> App<'a, 'b> {
    app.setting(ColorAuto)
        .setting(DeriveDisplayOrder)
        .setting(DisableHelpSubcommand)
        .setting(GlobalVersion)
        .setting(VersionlessSubcommands)
}
