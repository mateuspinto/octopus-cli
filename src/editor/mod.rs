use crate::action::Action;

use std::io;
use tui::backend::Backend;
use tui::layout::{Constraint, Direction, Layout, Rect};
use tui::style::{Color, Modifier, Style};
use tui::widgets::canvas::{Canvas, Line, Map, MapResolution, Rectangle};
use tui::widgets::{
    Axis, BarChart, Block, Borders, Chart, Dataset, Gauge, List, Marker, Paragraph, Row,
    SelectableList, Sparkline, Table, Tabs, Text, Widget,
};
use tui::{Frame, Terminal};

enum EditorMode {
    Show,
    Edit,
}

pub struct Editor<'a> {
    action: &'a Action,
    mode: EditorMode,
}

impl<'a> Editor<'a> {
    pub fn show(action: &Action) {
        let editor = Editor {
            action,
            mode: EditorMode::Show,
        };
    }

    pub fn edit(action: &Action) {
        let editor = Editor {
            action,
            mode: EditorMode::Edit,
        };
    }
}

pub fn draw<B: Backend>(terminal: &mut Terminal<B>, editor: &Editor) -> Result<(), io::Error> {
    terminal.draw(|mut f| {
        let chunks = Layout::default()
            .constraints([Constraint::Length(3), Constraint::Min(0)].as_ref())
            .split(f.size());
    });

    Ok(())
}
