use serde::{de, ser::SerializeMap, Deserialize, Deserializer, Serialize, Serializer};
use std::{
    collections::BTreeMap,
    error, fmt,
    fs::File,
    io::{BufReader, Read},
    path::Path,
};
use toml;
use toml::{value::Table, Value};

// Define action parsing errors
#[derive(Debug, Clone)]
pub enum Error<'a> {
    DoesNotExist(&'a str),
    Unparseable((usize, usize)),
    InvalidStructure,
}

type Result<'a> = std::result::Result<Action, Error<'a>>;

impl<'a> fmt::Display for Error<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::DoesNotExist(_) => write!(f, "the action does not exist"),
            Error::Unparseable(_) => write!(f, "couldn't parse the action"),
            Error::InvalidStructure => write!(f, "the action has an invalid structure",),
        }
    }
}

impl<'a> error::Error for Error<'a> {
    fn cause(&self) -> Option<&dyn error::Error> {
        None
    }
}

struct Param(BTreeMap<String, Value>);

#[derive(Serialize, Deserialize, Debug)]
struct Request {
    api: String,
    endpoint: String,
    method: String,
    data: Table,
}

#[derive(Serialize, Deserialize)]
pub struct ActionData {
    pub description: String,
    params: Param,
    requests: Vec<Request>,
}

pub struct Action {
    name: String,
    pub data: ActionData,
}

impl Action {
    pub fn from_path(path: &Path) -> Result {
        let name = path.file_stem().unwrap().to_str().unwrap();
        let file = match File::open(path) {
            Ok(file) => file,
            Err(_) => {
                return Err(Error::DoesNotExist(name));
            }
        };

        let mut buffered_reader = BufReader::new(file);
        let mut contents = String::new();

        buffered_reader
            .read_to_string(&mut contents)
            .expect("Error when buffering to string: ");

        match ActionData::from_str(&contents[..]) {
            Ok(data) => Ok(Action {
                name: String::from(name),
                data,
            }),
            Err(error) => Err(error),
        }
    }
}

impl<'a> ActionData {
    pub fn from_str(toml: &str) -> std::result::Result<Self, Error<'a>> {
        match toml::from_str(toml) {
            Ok(action) => Ok(action),
            Err(error) => {
                println!("{}", error);
                if let Some(lc) = error.line_col() {
                    use colored::*;
                    println!("{} {} {}:", "At line/col".blue().bold(), lc.0, lc.1);
                }

                Err(Error::Unparseable((0, 0)))
            }
        }
    }
}

impl Serialize for Param {
    fn serialize<S>(&self, serializer: S) -> std::result::Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let len = self.0.len();
        let mut map = serializer.serialize_map(Some(len))?;
        for (k, v) in &self.0 {
            map.serialize_entry(&k[..], &v)?;
        }
        map.end()
    }
}

impl<'de> Deserialize<'de> for Param {
    fn deserialize<D>(de: D) -> std::result::Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let params: BTreeMap<String, Value> = Deserialize::deserialize(de)?;

        // Check if all the elements are Table, so it's a key-only table
        let all_table = params.values().all(|v| match v {
            Value::Table(_) => true,
            _ => false,
        });

        // println!("All-table: {}", all_table);

        // Or it may also be all non-Table, so it's a valid param block
        let no_table = params
            .values()
            .find(|&v| match v {
                Value::Table(_) => true,
                _ => false,
            })
            .is_none();

        // println!("No-table: {}", no_table);

        Ok(Param(params))
    }
}
