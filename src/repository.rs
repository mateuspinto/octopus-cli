use colored::*;
use dialoguer::{Confirmation, Input, PasswordInput};
use git2::{
    build::RepoBuilder, Error, ErrorCode, FetchOptions, Progress, RemoteCallbacks, Repository,
    SubmoduleIgnore, SubmoduleStatus,
};
use indicatif::{ProgressBar, ProgressStyle};
use std::path::Path;

// Octopus repository URL
const URL: &str = "https://mateuspinto@bitbucket.org/unaspdigital/octopus.git";

// Define authentication tuple
type CredAuth = (String, String);

// Define a structure for common data for callbacks
pub struct CallbackOptions<'a> {
    pub progress_bar: &'a ProgressBar,
    pub auth: &'a mut Option<CredAuth>,
    pub prefix: &'a str,
}

// Clones both repository and sumodules
pub fn clone(path: &Path, modules: Option<Vec<String>>) -> Result<Repository, Error> {
    // Extract modules or empty
    let modules = modules.unwrap_or(vec![
        "actions".to_string(),
        "core".to_string(),
        "infra".to_string(),
    ]);

    // Auth starts as None but will mutate later to be reutilized
    let mut auth = None;

    // Define ProgressBar style and object
    let progress_style = ProgressStyle::default_bar()
        .template("{prefix:25.bold.dim} {wide_bar:.magenta} {pos:>5}/{len:5} [{percent}%]")
        .progress_chars("█▓▒░ ");

    let repo = {
        let progress_bar = ProgressBar::new(1);
        progress_bar.set_style(progress_style.clone());

        // Make data for callback
        let callback_options = CallbackOptions {
            progress_bar: &progress_bar,
            auth: &mut auth,
            prefix: "[ ] octopus",
        };

        // Clone repository
        let (result, opened) =
            open_or_clone_repository(URL, path, Some(callback_options), Some("next"));

        // Inform if octopus is already installed
        if opened {
            progress_bar.finish_and_clear();
            println!("[{}] octopus", "✔".green());
        } else {
            progress_bar.set_prefix(&format!("[{}] octopus", "✔".green()));
            progress_bar.finish();
        }

        result?
    };

    // Clone repository submodules
    for module in modules.iter() {
        match repo.submodule_status(module, SubmoduleIgnore::None) {
            Ok(SubmoduleStatus::IN_CONFIG) => {
                // Create progress bar
                let progress_bar = ProgressBar::new(1);
                progress_bar.set_style(progress_style.clone());

                // Make data for callback
                let callback_options = CallbackOptions {
                    progress_bar: &progress_bar,
                    auth: &mut auth,
                    prefix: &format!("[ ] {}", module),
                };

                // Clone repository
                clone_submodule(&repo, module, callback_options)?;

                progress_bar.set_prefix(&format!("[{}] {}", "✔".green(), module));
                progress_bar.finish();
            }
            Ok(status) => {
                if status.contains(
                    SubmoduleStatus::IN_INDEX
                        | SubmoduleStatus::IN_CONFIG
                        | SubmoduleStatus::IN_WD
                        | SubmoduleStatus::INDEX_ADDED,
                ) {
                    println!("[{}] {}", "✔".green(), module);
                } else {
                    println!("[{}] {}", "✘".red(), module);
                }
            }
            Err(err) => {
                println!(
                    "{} {} {} {}",
                    "Couldn't verify status of".red(),
                    module.red(),
                    "module:",
                    err.message()
                );
            }
        }
    }

    Ok(repo)
}

pub fn clone_submodule(
    repo: &Repository,
    name: &str,
    callback_options: CallbackOptions,
) -> Result<Repository, Error> {
    let mut submodule = repo.find_submodule(name)?;

    let submodule_repo = clone_repository(
        submodule.url().unwrap(),
        &repo.workdir().unwrap().join(submodule.path()),
        Some(callback_options),
        None,
    )?;

    submodule.add_finalize()?;

    Ok(submodule_repo)
}

pub enum RequireSubmodulesType {
    Prompt,
    Auto,
}

pub fn require_submodules(
    repo: &Repository,
    submodules: &[&str],
    type_: RequireSubmodulesType,
) -> Result<(), Error> {
    // Create vector for storing missing submodules names
    let mut missing = Vec::<&str>::new();

    // Check for submodules
    for name in submodules.iter() {
        // Try to open submodule
        let status = repo.submodule_status(name, git2::SubmoduleIgnore::None)?;

        //// DEBUG

        // let r = Repository::open(
        //     repo.path()
        //         .join("..")
        //         .join(repo.find_submodule(name)?.path()),
        // )?;

        // for status in r.statuses(None).unwrap().iter() {
        //     println!(
        //         "\nPath: {:#?}\nStatus: {:#?}\n",
        //         status.path(),
        //         status.status()
        //     );
        // }

        // let local = r.refname_to_id("refs/heads/master").unwrap();
        // let remote = r.refname_to_id("refs/remotes/origin/master").unwrap();

        // let behind = r.graph_ahead_behind(local, remote).unwrap();
        // println!("(ahead, behind): {:?}", behind);

        // let commit = r.find_commit(remote).unwrap();
        // let annotated = r.find_annotated_commit(remote).unwrap();

        // println!(
        //     "\ncommit: {:?}\nannotated: {:?}",
        //     commit.message(),
        //     annotated.id()
        // );

        // let analysis = r.merge_analysis(&[&annotated]).unwrap();
        // println!("Analysis: {:#?}\nPreference: {:#?}", analysis.0, analysis.1);

        // let merge = r.merge(&[&annotated], None, None).unwrap();

        // let mut remote = repo.find_remote("origin")?;

        // let mut remote_callbacks = RemoteCallbacks::new();

        // remote_callbacks.credentials(|repo: &str, user: Option<&str>, _| {
        //     // If no user is passed by git, prompt one
        //     let user = match user {
        //         Some(u) => String::from(u),
        //         None => Input::<String>::new()
        //             .with_prompt(&format!("User for '{}'", repo)[..])
        //             .interact()
        //             .unwrap(),
        //     };

        //     // Then, prompt password
        //     let password = &PasswordInput::new()
        //         .with_prompt(&format!("Password for '{}'", user)[..])
        //         .interact()
        //         .unwrap()[..];

        //     git2::Cred::userpass_plaintext(&user[..], password)
        // });

        // remote_callbacks.sideband_progress(|data| {
        //     use std::io::Write;
        //     print!("remote: {}", std::str::from_utf8(data).unwrap());
        //     std::io::stdout().flush().unwrap();
        //     true
        // });

        // // This callback gets called for each remote-tracking branch that gets
        // // updated. The message we output depends on whether it's a new one or an
        // // update.
        // remote_callbacks.update_tips(|refname, a, b| {
        //     if a.is_zero() {
        //         println!("[new]     {:20} {}", b, refname);
        //     } else {
        //         println!("[updated] {:10}..{:10} {}", a, b, refname);
        //     }
        //     true
        // });

        // // Here we show processed and total objects in the pack and the amount of
        // // received data. Most frontends will probably want to show a percentage and
        // // the download rate.
        // remote_callbacks.transfer_progress(|stats| {
        //     use std::io::Write;
        //     if stats.received_objects() == stats.total_objects() {
        //         print!(
        //             "Resolving deltas {}/{}\r",
        //             stats.indexed_deltas(),
        //             stats.total_deltas()
        //         );
        //     } else if stats.total_objects() > 0 {
        //         print!(
        //             "Received {}/{} objects ({}) in {} bytes\r",
        //             stats.received_objects(),
        //             stats.total_objects(),
        //             stats.indexed_objects(),
        //             stats.received_bytes()
        //         );
        //     }
        //     std::io::stdout().flush().unwrap();
        //     true
        // });

        // let mut fetch_options = FetchOptions::new();
        // fetch_options.remote_callbacks(remote_callbacks);

        // let fetch = remote.fetch(&[], Some(&mut fetch_options), None);

        // if let Err(e) = fetch {
        //     println!("Error on fetch: {}", e.message());
        // }

        // pb.finish();

        // let head = repo.head();

        // if let Err(e) = head {
        //     println!("Head error: {}", e.message());
        // } else if let Ok(r) = head {
        //     println!("Head: {:#?}", r.name().unwrap());
        // }

        // let sethead = repo.set_head("refs/head/next");

        // let checkout = repo.checkout_head(None);

        // if let Err(e) = checkout {
        //     println!("Checkout error: {}", e.message());
        // }

        println!("Debug {}: {:#?}", name, status);
        //// DEBUG

        if status.contains(
            SubmoduleStatus::IN_INDEX
                | SubmoduleStatus::IN_CONFIG
                | SubmoduleStatus::IN_WD
                | SubmoduleStatus::INDEX_ADDED,
        ) {
            println!("[{}] {}", "✔".green(), name);
        } else {
            println!("[{}] {}", "✘".red(), name);
            missing.push(name);
        }
    }

    // If no missing modules, return
    if missing.is_empty() {
        return Ok(());
    }

    // If prompt type and answered yes, proceed, else, return error
    if !match type_ {
        RequireSubmodulesType::Prompt => Confirmation::new()
            .with_text(&format!(
                "Do you want to download module(s) {}?",
                missing.join(", ")
            ))
            .interact()
            .unwrap(),
        _ => true,
    } {
        return Err(Error::from_str("User has aborted the process."));
    }

    // Auth starts as None but will mutate later to be reutilized
    let mut auth = None;

    // Define ProgressBar style and object
    let progress_style = ProgressStyle::default_bar()
        .template("{prefix:25.bold.dim} {wide_bar:.magenta} {pos:>5}/{len:5} [{percent}%]");

    // Try to clone submodules
    for name in missing.iter() {
        // Safely unwrap as it's managed internally
        let mut submodule = repo.find_submodule(name).unwrap();

        // Make progress bar
        let progress_bar = ProgressBar::new(1);
        progress_bar.set_style(progress_style.clone());

        // Make data for callback
        let callback_options = CallbackOptions {
            progress_bar: &progress_bar,
            auth: &mut auth,
            prefix: &format!("[ ] {}", name),
        };

        // Clone repository
        clone_repository(
            submodule.url().unwrap(),
            &repo.path().join("..").join(submodule.path()),
            Some(callback_options),
            None,
        )?;

        // Finish progress bar
        progress_bar.set_prefix(&format!("[{}] {}", "✔".green(), name));
        progress_bar.finish();

        // Add submodule to index
        submodule.add_finalize()?;
    }

    Ok(())
}

// Clone a repository to path based on url and callback options
fn clone_repository<'a>(
    url: &str,
    path: &Path,
    callback_options: Option<CallbackOptions<'a>>,
    branch: Option<&str>,
) -> Result<Repository, Error> {
    let mut repo = RepoBuilder::new();

    if let Some(callback_options) = callback_options {
        let remote_callbacks = callbacks(callback_options);

        let mut fetch_options = FetchOptions::new();
        fetch_options.remote_callbacks(remote_callbacks);

        repo.fetch_options(fetch_options);
    }

    repo.branch(branch.unwrap_or("master"));

    repo.clone(url, path)
}

// Try to open reposiotry; if not available, clone to path based on url and callback options
fn open_or_clone_repository<'a>(
    url: &str,
    path: &Path,
    callback_options: Option<CallbackOptions<'a>>,
    branch: Option<&str>,
) -> (Result<Repository, Error>, bool) {
    //  Try to open repository
    let repo = Repository::open(path);

    match repo {
        Ok(repo) => return (Ok(repo), true),
        Err(err) => {
            if err.code() != ErrorCode::NotFound {
                return (Err(err), false);
            }
        }
    }

    // If couldn't open, try to clone
    let mut repo = RepoBuilder::new();

    if let Some(callback_options) = callback_options {
        let remote_callbacks = callbacks(callback_options);

        let mut fetch_options = FetchOptions::new();
        fetch_options.remote_callbacks(remote_callbacks);

        repo.fetch_options(fetch_options);
    }

    repo.branch(branch.unwrap_or("master"));

    (repo.clone(url, path), false)
}

// Callback common function to be used on clones
fn callbacks<'a>(callback_options: CallbackOptions<'a>) -> RemoteCallbacks<'a> {
    let mut callbacks = RemoteCallbacks::new();
    let CallbackOptions {
        progress_bar,
        auth,
        prefix,
    } = callback_options;

    // Prompt for credentials if needed, and return new cred
    callbacks.credentials(move |repo: &str, user: Option<&str>, _| match auth {
        Some(auth) => git2::Cred::userpass_plaintext(&auth.0[..], &auth.1[..]),
        None => {
            // If no user is passed by git, prompt one
            let user = match user {
                Some(u) => String::from(u),
                None => Input::<String>::new()
                    .with_prompt(&format!("User for '{}'", repo)[..])
                    .interact()
                    .unwrap(),
            };

            // Then, prompt password
            let password = &PasswordInput::new()
                .with_prompt(&format!("Password for '{}'", user)[..])
                .interact()
                .unwrap()[..];

            // Here we set the passed reference so it can be used again later
            *auth = Some((user.clone(), password.to_string()));

            git2::Cred::userpass_plaintext(&user[..], password)
        }
    });

    // Update progress bar according to transfer progress
    callbacks.transfer_progress(move |p: Progress| {
        progress_bar.set_prefix(prefix);
        progress_bar.set_length(p.total_objects() as u64);
        progress_bar.set_position(p.received_objects() as u64);
        progress_bar.tick();

        true
    });

    callbacks
}
