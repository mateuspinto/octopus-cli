use clap::ArgMatches;
use colored::*;
use git2::Repository;
use std::{io, path::Path};
use walkdir::{DirEntry, WalkDir};

pub fn main(matches: Option<&ArgMatches>, repo: &Repository) {
    // Match --local flag
    let local: bool = match matches {
        Some(matches) => matches.is_present("local"),
        None => false,
    };

    // Match --namespace argument
    let namespace: Option<&str> = match matches {
        Some(matches) => matches.value_of("namespace"),
        None => None,
    };

    // If no --local passed, check for differences between local and remote
    if !local {
        sync();
    }

    // Create path based on namespace
    let path = repo.path().join("..").join(match namespace {
        Some(namespace) => {
            println!(
                "{} {}:",
                "Listing actions for".bold().bright_blue(),
                namespace.bright_blue().bold().underline()
            );
            format!("actions/{}", namespace)
        }
        None => {
            println!(
                "{}",
                format!("Listing {} actions:", "all".underline())
                    .bold()
                    .bright_blue()
            );
            "actions".to_string()
        }
    });

    print!("\n");

    // List actions or print error if didn't work
    if let Err(err) = list_actions(&path) {
        println!("Error when trying to list_actions: {:#?}", err);
    }

    ()
}

fn sync() {}

fn list_actions(dir: &Path) -> io::Result<()> {
    // Walk through directory searching for actions. Won't match hidden nor root
    let walker = WalkDir::new(dir).min_depth(1).into_iter();
    for entry in walker.filter_entry(|e| !is_hidden(e)) {
        let entry = entry?;
        let path = entry.path();

        // Print action name and description
        if path.is_file() {
            // Print namespace
            let relative_path = &(path
                .to_str()
                .unwrap()
                .trim_start_matches(dir.to_str().unwrap()))[1..];

            let name = relative_path
                .replace("/", ".")
                .trim_end_matches(".toml")
                .to_string();

            println!("{}", name.bold());

            // Print description or invalid
            let action = crate::action::Action::from_path(path);
            let message = match action {
                Ok(action) => action.data.description,
                Err(err) => format!("{}", err).red().to_string(),
            };

            println!("{}\n", message);
        }
    }

    Ok(())
}

fn is_hidden(entry: &DirEntry) -> bool {
    entry
        .file_name()
        .to_str()
        .map(|s| s.starts_with("."))
        .unwrap_or(false)
}
