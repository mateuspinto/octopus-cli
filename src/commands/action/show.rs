use crate::action::{Action, Error};
use clap::ArgMatches;
use colored::*;
use git2::Repository;
use std::{io, path::Path};
use walkdir::{DirEntry, WalkDir};

pub fn main(matches: Option<&ArgMatches>, repo: &Repository) {
    // Match --local flag
    let local: bool = match matches {
        Some(matches) => matches.is_present("local"),
        None => false,
    };

    // Match --namespace argument
    let action_name: &str = matches.unwrap().value_of("action name").unwrap();

    // If no --local passed, check for differences between local and remote
    if !local {
        sync();
    }

    // Get actions path
    let path = &repo
        .path()
        .parent()
        .unwrap()
        .join(&repo.find_submodule("actions").unwrap().path())
        .join(format!("{}.toml", action_name));

    println!("Path: {:?}", path);

    // Open action to be shown
    let action = Action::from_path(path);

    // If didn't work, print error and return
    if let Err(err) = action {
        match err {
            Error::DoesNotExist(name) => println!(
                "{} {} {}",
                "The action".red().bold(),
                name.red().bold(),
                "does not exists!".red().bold()
            ),
            Error::Unparseable(_) => println!(
                "{}",
                "Couldn't parse the action. You may use `octopus action validate <action>` to see what is wrong with the action.".red().bold()
            ),
            Error::InvalidStructure => println!("{}", "This action has invalid structure. See #DOCUMENTATION#".red().bold()),
        }

        return ();
    }

    // Show

    ()
}

fn sync() {}
