mod list;
mod show;

use crate::repository;
use clap::ArgMatches;
use colored::*;
use git2::Repository;
use repository::{require_submodules, RequireSubmodulesType};

pub fn main(matches: Option<&ArgMatches>, repo: Repository) {
    println!("Checking for neccessary modules...");
    if let Err(err) = require_submodules(&repo, &["actions"], RequireSubmodulesType::Prompt) {
        println!(
            "{} {}",
            "Couldn't install required submodules:".red(),
            err.message().red()
        );
    }

    match matches {
        Some(matches) => match matches.subcommand_name() {
            Some("list") => list::main(matches.subcommand_matches("list"), &repo),
            Some("show") => show::main(matches.subcommand_matches("show"), &repo),
            _ => (),
        },
        None => (),
    }
}
