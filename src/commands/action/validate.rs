use crate::action;

use clap::ArgMatches;
use std::{
    fs,
    fs::File,
    io,
    io::{BufReader, Read},
    path::{Path, PathBuf},
};

pub fn main(matches: Option<&ArgMatches>, path: &Path) {
    let verbose = if let Some(m) = matches {
        m.is_present("verbose")
    } else {
        false
    };

    let actions_dir = path.join("actions");

    if actions_dir.exists() == false {
        println!("The actions folder has not been found! Are you sure your Octopus has been correctly initialized?");
        return ();
    }

    let result = validate_actions(actions_dir);

    match result {
        Ok(_) => (),
        Err(e) => println!("An error ocurred when validating actions: {}", e),
    };
}

fn validate_actions(dir: PathBuf) -> io::Result<()> {
    if dir.is_dir() {
        for entry in fs::read_dir(dir)? {
            let entry = entry?;
            let path = entry.path();
            if path.is_dir() {
                validate_actions(path)?;
            } else {
                return validate_single_file(path);
            }
        }
    }
    Ok(())
}

fn validate_single_file(entry: PathBuf) -> io::Result<()> {
    let file = File::open(&entry)?;
    let mut buffered_reader = BufReader::new(file);
    let mut contents = String::new();

    buffered_reader
        .read_to_string(&mut contents)
        .expect("Error when buffering to string: ");

    // println!("{}:\n{}", &entry.display(), contents);

    validate_action(&contents[..]);

    Ok(())
}

fn validate_action(toml: &str) {
    let a = toml::from_str::<action::Action>(toml);

    println!("{:#?}", a);
}
