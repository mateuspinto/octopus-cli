use crate::repository;
use clap::ArgMatches;
use dialoguer::{theme::ColorfulTheme, Checkboxes};
use std::{error::Error, fs, path::Path};

pub fn main(matches: Option<&ArgMatches>, path: &Path) {
    let mut modules = None;

    if let Some(m) = matches {
        if m.is_present("force") {
            if let Err(e) = fs::remove_dir_all(path) {
                println!("Couldn't remove Octopus directory: {:?}", e.description());
            };
        }

        if m.is_present("modules") {
            modules = m.values_of_lossy("modules");
        } else if m.is_present("select") {
            let items = vec!["actions", "core", "infra"];
            let options = Checkboxes::with_theme(&ColorfulTheme::default())
                .items(&items)
                .with_prompt(
                    "Check (by pressing spacebar) the modules you want to install with Octopus",
                )
                .interact()
                .unwrap();

            // Create container for modules
            let mut result = Vec::<String>::new();

            // Filter based on options
            for (i, item) in items.iter().enumerate() {
                if options.iter().any(|idx| *idx == i) {
                    result.push(item.to_string());
                }
            }

            // Finally, set modules
            modules = if result.is_empty() {
                None
            } else {
                Some(result)
            }
        }
    }

    match modules {
        None => println!("Initializing Octopus with all modules..."),
        Some(ref vec) => {
            if vec.is_empty() {
                println!("Initializing Octopus with no modules...");
            } else {
                println!("Initializing Octopus and module(s) {}.", vec.join(", "));
            }
        }
    }

    if let Err(err) = repository::clone(path, modules) {
        println!("Couldn't install Octopus on your system: {}", err.message());
    };

    ()
}
